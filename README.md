# What is Libre?

### A new blockchain focused on Bitcoin DeFi.

Libre makes Bitcoin and Tether fast, programmable, and actually easy to use.

Libre features simple usernames, simple user interfaces and simple inter-chain connectivity to make Bitcoin and Tether accessible to anyone, anywhere.

Libre is a delegated-proof-of-stake blockchain, with non-custodial pegging of assets provided by PNetwork, and simple on-and-off ramps to Bitcoin and Lightning Network. It's completely decentralized, with no central point of failure.

Libre is a collective effort. It combines technologies built by a collective of developers and Bitcoin-enthusiasts from the USA, Italy, UK, Costa Rica, Argentina, Georgia, Lithuania, and Serbia. It also has [nodes all around the world](https://libre.antelope.tools/nodes-distribution).&#x20;

## Simple

Libre is the easiest blockchain to use and build on - accounts are free and you do not need to acquire tokens to use Libre. The speed, reliability, and cost of transactions is equal to the utility that people around the world have previously only been able to enjoy when using centralized applications - now Libre chain provides that level of functionality.

## Fair

There are no LIBRE coins allocated to team, investors, or funds. The LIBRE token distribution starts with an [airdrop](the-libre-coin/spindrop/) and block mining rewards that are paid to the elected validators for each block processed. LIBRE holders can engage in [governance](broken-reference) through the DAO and through the [validator elections](governance/validator-election.md).

## Free

Most of all, Libre is about freedom. The freedom to transact, trade, build, [earn](broken-reference) and self-govern. The etymology of the name Libre comes from the latin _liber_ meaning "free, unrestricted."&#x20;
