---
description: Creating a Libre (Testnet) account using Anchor wallet.
---

# Creating a Libre Testnet Account in Anchor

First you will need to [download and install The Anchor Wallet (desktop edition).](./)

### Generate Key in Anchor

Go to Tools > Manage Keys

<figure><img src="../../.gitbook/assets/image (5).png" alt=""><figcaption></figcaption></figure>

Click Generate Key Pairs (_if you do not see Tools, try selecting another chain in Anchor)_

<figure><img src="../../.gitbook/assets/image (14).png" alt=""><figcaption><p>Alternatively, you can create a key using Ledger hardware wallet if you enable Ledger support in Anchor.</p></figcaption></figure>

Click Save Keys to Wallet - do not close the window yet. We will need this public key in the next step.

<figure><img src="../../.gitbook/assets/image (17).png" alt=""><figcaption></figcaption></figure>

### Create Account with Public Key

Copy one of the public keys you created in Anchor by selecting it and hitting CMD+C (CNTRL+C on Windows)

<figure><img src="../../.gitbook/assets/image (16).png" alt=""><figcaption></figcaption></figure>

In a web browser, go to Faucet on Testnet [https://libre-testnet.antelope.tools/faucet](https://libre-testnet.antelope.tools/faucet)

Paste in Public Key and enter an account name (must be unique).

<figure><img src="../../.gitbook/assets/image (12) (1).png" alt=""><figcaption></figcaption></figure>

Click “Create” and you will see a success notification on the bottom.

<figure><img src="../../.gitbook/assets/image (18).png" alt=""><figcaption></figcaption></figure>

Back in Anchor, select the Libre (Testnet) by going to "Manage Blockchains"

<figure><img src="../../.gitbook/assets/image (8).png" alt=""><figcaption><p><em>if Libre is not showing under "Manage Blockchains" - then follow the instructions here to add it</em> <a href="https://cryptobloks.gitbook.io/using-anchor-wallet-with-the-libre-blockchain/"><em>https://cryptobloks.gitbook.io/using-anchor-wallet-with-the-libre-blockchain/</em></a></p></figcaption></figure>



Click “Home > Scan for Accounts” and you will see a list of accounts that match your key.

Choose the @active and @owner roles then click import.

![](<../../.gitbook/assets/image (6) (1).png>)

Congrats! Now your account is imported in Anchor and your private key is stored in Anchor.

Next, try to get some [Testnet LIBRE tokens](getting-testnet-libre-tokens.md).





