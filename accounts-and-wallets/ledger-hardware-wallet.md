---
description: Using a Ledger with Libre chain.
---

# Ledger Hardware Wallet

The Ledger hardware wallet can be used to store your Libre private keys and handle signing requests both online and offline. It has the ability to import and transmit transactions.

The best way to do this is to use [The Anchor Wallet](anchor-wallet/). You can generate your keys on the Ledger and [connect the Ledger to Anchor](https://samveku.medium.com/how-to-add-eos-account-to-your-ledger-nano-x-s-using-anchor-wallet-79ae9917cf53).



**7 Simple Steps**

1. Install EOS app on Ledger (Libre uses the same derivation path as EOS).
2. Install [Anchor Desktop Wallet](anchor-wallet/) (v1.3.8+)
3. Enable Ledger Support in Anchor (first image below)
4. Load Key from Ledger
5. Copy Public Key from Anchor and [Create Libre Account](https://accounts.libre.org) on web
6. Scan for Accounts in Anchor
7. Select the @active role (second image below)

<figure><img src="../.gitbook/assets/1_kT2wJ30x7SrgjY3XSdvtmg.jpeg" alt=""><figcaption><p>Anchor Wallet (desktop) allows you to secure your Libre account using Ledger Hardware Wallet.</p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (4).png" alt=""><figcaption><p>Example of importing account using a key stored on the Ledger.</p></figcaption></figure>
