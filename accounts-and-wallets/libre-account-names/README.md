---
description: >-
  Libre uses simple Account Names controlled by cryptographic keys and
  permissions.
---

# LIBRE Account Names

LIBRE account names are like Twitter account names: short human readable names such as "cryptoguy" or "billsmith" that can be used to create "pseudonymous" identities on chain. They can be anywhere from 1 to 12 characters and can contain the letters a thru z and numbers 1,2,3,4,5. The numbers 0,6,7,8, and 9 are not permitted for technical reasons.

Libre accounts are protected by cryptographic public / private keypairs. Your private key is your password - do not share it with anyone or they will be able to control your account.\
\
There is no requirement that your LIBRE account name be in any way associated with your real name. You can be "Frank White" and have the account name "bcbny5amqcnt" or "guitarguy".&#x20;

You can easily have multiple accounts. Note that all activity is recorded on the public blockchain, and, just like you wouldn't want your tax information shared with the world, privacy of transactions is generally a good idea.\
\
More advanced permissions are available - both single keys and multisig keys or other accounts. Libre also supports using multiple wallets including [Bitcoin Libre](../bitcoin-libre-wallet.md), [Anchor](../anchor-wallet/), and [Ledger Hardware Wallet](../ledger-hardware-wallet.md).&#x20;

\*\*You can also [generate your own keys or seed phrase](cryptographic-keys.md) for your Libre account.

allows to create custom hierarchical permissions that stem from the owner permission. A custom permission is basically a key that can only perform the actions you allow it to perform.

