# USDT / Ethereum

The USDT on Libre is PUSDT. PUSDT is transparent, decentralized, and censorship-free. PUSDT utilizes [PNetwork](pnetwork.md) oracle technology which has cross-chain liquidity to many other chains and is not dependent on Libre validators or governance.

The PNetwork bridge supports PUSDT **(Libre) > USDT (ETH)** and **USDT (ETH) > PUSDT (Libre)**

[https://dapp.ptokens.io/swap?asset=usdt\&from=eth\&to=libre](https://dapp.ptokens.io/swap?asset=usdt\&from=eth\&to=libre)

