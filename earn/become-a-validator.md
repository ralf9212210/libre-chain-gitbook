---
description: Become a Validator on Libre Chain.
---

# Become a Validator

Anyone can become a Libre validator to earn LIBRE for processing Libre chain transactions and putting those into blocks.&#x20;

The first step is to get a node running on the Libre Testnet following the [guide here on Gitlab](https://gitlab.com/libre-chain/libre-chain-nodes).&#x20;

The Testnet [Telegram](https://t.me/+l-Un\_5tmHzZkNzhh) and [Discord](https://discord.com/invite/asJNsB6sYJ) are good places to begin as a validator and get support with setting up your node. Good vibes only please.

For information about Validator pay see [validators.md](../the-libre-blockchain/validators.md "mention").
