---
description: DAO tax is applied to all claims
---

# DAO Tax

High staking returns are inherently built into the Libre blockchain. This is inflationary by design, but becomes less inflationary over time. It is also inflation that is generated directly out of code -- so that, from a user's perspective **if they are expecting a 10% APY, they will be guaranteed a 10% APY in unit terms**.&#x20;

In other words, new tokens are minted systematically for every stake, as opposed to generated as a whole for the chain and then apportioned to stakers.\
\
With this in mind, the token economics have been designed that every "mint" of LIBRE tokens corresponding to a stake results in a 10% mint to the DAO. You can think of this as a "**sustainability tax**" for community projects which include subsidizing wallets, play to earn games etc.. Every such spend must be voted and approved by the DAO. The DAO voting is based on the amount of LIBRE that a user has actively staked. This will be constantly recalculated by the [DAO smart contract](https://gitlab.com/libre-chain/btc-libre-governance).\
\
