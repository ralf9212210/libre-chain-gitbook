---
description: Farming on Libre Chain
---

# Farming

Anyone can provide liquidity to the Libre AMM. As an incentive, they will be able to earn LIBRE rewards by staking their LP tokens. For example, the PUSDT / PBTC pool will accrue farming rewards equal to 1/4 of whatever validators earn per day.

To add liquidity, visit [https://dashboard.libre.org/swap](https://dashboard.libre.org/swap)&#x20;

Farming APYs will depend on the number of participants farming each pool and the share of staked LP tokens that each account has staked.
