# Play To Earn

LIBRE was built, from the beginning around the idea of "Play to Earn", with the Bitcoin Libre wallet being an extremely simple version of that: spin a wheel in exchange for a small amount of Bitcoin.\
\
The Libre ecosystem will encourage games and wallets to hand out free tokens to their users by getting subsidized by the DAO. An example "Bitcoin Wordle" game is shown as an illustration from the company "Machine Candy".  We encourage other such companies.

* [Bitcoin Wordle](https://wordle.machinecandy.com/)
