---
description: Libre DAO - propose and vote
---

# Libre DAO

The DAO is the Decentralized Autonomous Organization which has a treasury and can allocate funds from the treasury for proposals made by token holders to improve the chain or build / integrate applications. It works on the process of 1 token (LIBRE) \* staked period = voting power.

By design, the DAO does not have power to change the core constants and smart contracts of Libre. That power lies in the validator multisig which requires 2/3+1 vote of all active validators. You can view these code changes here under [libre-mainnet-code-update-proposals](libre-mainnet-code-update-proposals/ "mention").

## Libre DAO Operations

The general operation of the DAO is that proposals are made, voted on for a multiple day period and then either passed or rejected.&#x20;

The DAO requires at least 10% of all [Voting Power](voting-power.md) to be voted on a proposal -  51% of the voting power for / against will decide.

Once a proposal is passed, the validators must sign a 2/3+1 multisig to distribute funds from the DAO treasury.

The DAO is perpetually funded from the [dao-tax.md](../earn/dao-tax.md "mention").
