---
description: December 2022 proposal
---

# December 2022

### Summary

Changes eosio permissions for the existing stake permission to add reward.libre the ability to transfer and issue LIBRE specifically for farming rewards in the LP tokens staked from swap.libre

#### Requested signatories&#x20;

quantum, edeniaedenia, blocktime, cryptobloks, cryptolions, draperx, eosphere, eosusa, genereos, goodblock,heliosblocks, hotstart, interchainbp, libremt, librenodeone, libretech, rioblocks, sweden, teamgreymass, zenhash, zuexeuz, uberleap, echobasefour, detroit, blokcrafters, amsterdam

### Relevant Code

It is already deployed and tested on Libre Testnet.

The relevant code for the reward contract can be viewed here:

[https://gitlab.com/libre-chain/staking-contract/-/blob/testnet/reward/src/providers.cpp](https://gitlab.com/libre-chain/staking-contract/-/blob/testnet/reward/src/providers.cpp)

![](https://lh3.googleusercontent.com/rzZzyM3k\_K3IfXYvURy8WED-W1gDmoFJpLbvFORxvlYlrsc3YNCfzs08\_xJKMtKDiK1LvXWZpUHJXZWqqdo-GgBjheLd8D0vfsFbtP2i6CrqWzWbNQ5M1WVxxCt5eFqlOvgwVGnJnPKFbv0eO05raTM)

\
\


### Multisigs

1. Add reward.libre to existing permission - link: [uvv1oa](https://local.bloks.io/msig/quantum/uvv1oa?nodeUrl=http%3A%2F%2Flb.libre.org\&coreSymbol=LIBRE\&systemDomain=eosio\&hyperionUrl=http%3A%2F%2Flb.libre.org)
2. limits the permissions to only eosio.token::issue and eosio.token::transfer (already set now, but likely needs to be set again after adding reward.libre) - link: [lvv1oa](https://local.bloks.io/msig/quantum/lvv1oa?nodeUrl=http%3A%2F%2Flb.libre.org\&coreSymbol=LIBRE\&systemDomain=eosio\&hyperionUrl=http%3A%2F%2Flb.libre.org)

\
