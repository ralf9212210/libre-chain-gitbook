# March 2023

### Multisig 1/x - "[daoperm](https://msig.app/libre/quantum/daoperm)"

#### Link&#x20;

[https://msig.app/libre/quantum/daoperm](https://msig.app/libre/quantum/daoperm)

#### Summary

Updates active and owner permissions of dao.libre to be controlled by 2/3+1 validators to update code, countvotes, and approve or cancel (veto) proposals.&#x20;

### Requested signers

all active producers

### Code Changes

none

### Multisig 2/x - “[returnfunds1](https://msig.app/libre/blocktime/returnfunds1)”

[https://msig.app/libre/blocktime/returnfunds1](https://msig.app/libre/blocktime/returnfunds1)

#### Summary

Return funds accidentally sent to system account.&#x20;

### Requested signers

all active producers

### Code Changes

none

### Multisig 3/x - “systemupdate”

#### Summary

Updates eosio to reference voting power in stake.libre::power for choosing validators

[needs to be proposed](https://local.bloks.io) after full testing on testnet

### Requested signers

all active producers

### Code Changes

**Repo:** [https://gitlab.com/libre-chain/libre-chain-contracts](https://gitlab.com/libre-chain/libre-chain-contracts)****

**Path to WASM in repo: TBD**

| <p><br></p> | Currently Deployed | Proposed Update |
| ----------- | ------------------ | --------------- |
| Commit hash |                    |                 |
| sha256sum   |                    |                 |

**Code comparison (proposed to deployed):**

TBD
