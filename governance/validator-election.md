# Validator Election

The Libre chain is a delegated-proof-of-stake blockchain. Staked LIBRE can elect validators who secure the network by processing transactions into blocks. Validators also have collective control of the eosio.prods permission which can change core functionality with a 2n/3 + 1 multisig. While we discourage changing any parts of Libre, it is technically possible to patch or change smart contracts using this permission. Therefore, voting is an essential element to running Libre.

Validators are professional and highly technical node operators who run the core LIBRE blockchain code - they independently verify that each transaction is valid (no double spends). It's critical that at least 10 validators validate each block, otherwise the 2n/3+1 rule for block validation might fail and the entire chain will stop.

{% hint style="info" %}
Selecting validators is critical. A list of all validators is provided by the dashboard (located at dashboard.libre.org), and as a simplification, each wallet can vote for a single validator. If you have a lot of tokens, and you wish to spread your voting over multiple validators, simply create multiple accounts.


{% endhint %}
