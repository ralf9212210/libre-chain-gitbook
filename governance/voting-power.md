# Voting Power

Each individual stake has a voting weight that is calculated daily for every staked account on Libre chain.&#x20;

This is called "voting power" and it is calculated based on the remaining days left in a stake up to 1 year.&#x20;

The formula is here:

$$
vp = (staked LIBRE) * (1 + MIN(1,(stakeLength /  365))
$$

If you have 1000 tokens and you have 365 days remaining, your voting power will be 2000.&#x20;

If you only have 10 days remaining, your voting power will be 1027.39726.

This formula is implemented in the [open-source taking contract](https://gitlab.com/libre-chain/staking-contract) and cannot be updated without a hard fork of the code.
