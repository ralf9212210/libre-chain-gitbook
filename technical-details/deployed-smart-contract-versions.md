---
description: >-
  Tracks commits and sha hashes of deployed smart contracts on mainnet and
  testnet.
---

# Deployed Smart Contract Versions

|                                                                      | commit hash (testnet)                    | sha256sum (testnet)                                              | commit hash (mainnet)                    | sha256sum (mainnet)                                              |
| -------------------------------------------------------------------- | ---------------------------------------- | ---------------------------------------------------------------- | ---------------------------------------- | ---------------------------------------------------------------- |
| [mint.libre](https://gitlab.com/libre-chain/staking-contract)        | cc6a94ea7afd3c5f9567ee4ceaa54ca967b7fa41 | b4877cd9d6c15f0adef8e56796d0db735dc47bbbb9eb7df28581e295b4e8545e | cc6a94ea7afd3c5f9567ee4ceaa54ca967b7fa41 | b4877cd9d6c15f0adef8e56796d0db735dc47bbbb9eb7df28581e295b4e8545e |
| [stake.libre](https://gitlab.com/libre-chain/staking-contract)       | 9c3a3bc7393afc052e94122ffbaa7bca1dcfe37b | 6962b0ee319957fb1783fcdcf8fa5ed029867bf1e7208522694664bd6607fa98 | 539476403d1c6f86d1e1176f41aceef21a0cd118 | e285a511028be0dd73c5bd6090ebe775b7d2565c8682e90b30afa5636636c896 |
| [reward.libre](https://gitlab.com/libre-chain/staking-contract)      | 4ecbfaf44898ee8123d936babd2a488d0d198d36 | 0ca7ce0d25d703e4384fc531a2211e6af1683df05d328c31ebc237818aced171 | 4ecbfaf44898ee8123d936babd2a488d0d198d36 | 0ca7ce0d25d703e4384fc531a2211e6af1683df05d328c31ebc237818aced171 |
| [farm.libre](https://gitlab.com/libre-chain/staking-contract)        | 5be127baeec938a5179df039c187ce3fd80cfaee | 0b8f5753077fe95fd2cf528208d0c49b62c8c1aec8e41ba8615c03c31403150c | 5be127baeec938a5179df039c187ce3fd80cfaee | 0b8f5753077fe95fd2cf528208d0c49b62c8c1aec8e41ba8615c03c31403150c |
| [dao.libre](https://gitlab.com/libre-chain/btc-libre-governance)     | 2ca6084e6f81eb40d19c00c13220ad07bfae57a5 | c1dd03c31b82a5bc82f5a9b8d4c9cebf909f7db3953f06029ab5bf8d8bd1cb37 | 2ca6084e6f81eb40d19c00c13220ad07bfae57a5 | c1dd03c31b82a5bc82f5a9b8d4c9cebf909f7db3953f06029ab5bf8d8bd1cb37 |
| [eosio.msig](https://gitlab.com/libre-chain/libre-chain-contracts)   | 86acd956f15659cab10fb99df62c4a8420b8c805 | faa87ac1cbf719544f32de5e652e72b0080bca2ddede44ad1a104e3c5768b225 | 4c412311cc188438de2c5b4a7701921ec41c7ee8 | 1040fbe8e491ab6038b64b4675310ebcf9218dc1e69ec2712ef0b11db687c8ff |
| [eosio.libre](https://gitlab.com/libre-chain/libre-chain-contracts)  |                                          |                                                                  |                                          |                                                                  |
| [system.libre](https://gitlab.com/libre-chain/libre-chain-contracts) |                                          |                                                                  |                                          |                                                                  |
| [accts.libre](https://gitlab.com/libre-chain/libre-referrals)        |                                          |                                                                  |                                          |                                                                  |
