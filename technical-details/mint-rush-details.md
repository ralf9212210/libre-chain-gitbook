---
description: Libre Mint Rush formulas.
---

# Mint Rush Details

The Libre Mint Rush was a distribution of LIBRE in exchange for contributions to the AMM liquidity pool of LIBRE / BTC. For graphs and logic please see [mint-rush.md](../the-libre-coin/mint-rush.md "mention").

### Mint Rush Math

The Mint APY was calculated using the following formula:

$$f(StakedDays) =((Alpha0 + √ (Staked Days/365)*(Beta0-Alpha0)))*(1 + Mint Bonus)$$

Alpha0 and Beta0 are defined in the [libre-core-constants.md](libre-core-constants.md "mention").

All of the values are stored on-chain as defined in the Mint Rush Table below in the [open-source Libre staking contract](https://gitlab.com/libre-chain/staking-contract).

### Mint Rush LP Token Repayment

Mint Rush contributors received staked LIBRE and also slowly get  back their share of the initial liquidity pool which includes the BTC, matching LIBRE, and any fees that have accumulated.

The rate of repayment (daily) is calculated based on the total LP tokens that of any account (that contributed to the mint rush liquidity pool) using the following formula:

$$
r = (Total LP Tokens) / (LP Repayment Length)
$$



### Mint Rush Table

| Variable                      | Type  | Value                                                                    |
| ----------------------------- | ----- | ------------------------------------------------------------------------ |
| Index                         | INT   | Table row index                                                          |
| Day of Mint Rush              | DATE  | =Todays Date - Mint Start Date                                           |
| Staked Days                   | INT   | User Defined \*\*Required\*\*                                            |
| Your BTC Contribution         | FLOAT | =User Defined \*\*Required\*\*                                           |
| Mint Bonus                    | %     | =(1 - ( Day of Mint Rush / Length of Mint Rush ))                        |
| Libre Minted                  | FLOAT | =( Total BTC Contributed / Your BTC Contribution ) \* Total Libre Minted |
| Mint APY                      | %     | =((Alpha0 + sqrt(Staked Days/365)\*(Beta0-Alpha0)))\*(1 + Mint Bonus)    |
| Total BTC Contributed         | FLOAT | =SUM of all Bitcoin Contributed During Mint Rush                         |
| Total Libre Minted            | FLOAT | =200,000,000                                                             |
| Mint Bonus                    | INT   | =2x                                                                      |
| Length of Mint Rush (Days)    | INT   | =30                                                                      |
| Mint Start Date               | DATE  | 11/15/2022                                                               |
| Minimum Staking Period (Days) | INT   | =30                                                                      |
| LP Repayment Length (Days)    | INT   | =365                                                                     |

