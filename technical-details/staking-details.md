# Staking Details

### Libre Staking Math

The staking return is calculated using the following formula:\
$$f(s,t) = min(t) + √ (s) * ( Max(t)-Min(t) )$$\
\
Where `s` is the index of time elapsed since day one of staking, `t` is the staking length.&#x20;

`Min` is a function of time that decreases with `t`, and `Max` is also a function of `t.`&#x20;

The degradation of `Min` and `Max` is also a function of the square root of time `s` and not `t`.\
****\
****These are core in the inherent game mechanics of the LIBRE token incentives.

This math is implemented in the [open-source Libre staking contract](https://gitlab.com/libre-chain/staking-contract).

### Libre Staking Table

| Variable     | Type  | Value                                                                                                                                                                                          |
| ------------ | ----- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Stake Date   | DATE  | Date of User-Initiated Stake                                                                                                                                                                   |
| Stake Length | INT   | User Defined \[1-1460 DAYS] \*\*Required\*\*                                                                                                                                                   |
| Mint Bonus   | %     | Only applies to stakes created in Mint                                                                                                                                                         |
| Libre Staked | FLOAT | User Defined \*\*Required\*\*                                                                                                                                                                  |
| APY          | %     | =((Alpha0+sqrt(StakeLength/365)\*(Beta0-Alpha0))+((AlphaT+sqrt(StakeLength/365)\*(BetaT-AlphaT))-(Alpha0+sqrt(StakeLength/365)\*(Beta0-Alpha0)))\*sqrt(MIN(1,(StakeDate-L))/T))\*(1+MintBonus) |
| Payout       | FLOAT | =(StakeLength/365) \* LibreStaked \* APY + Libre Staked                                                                                                                                        |
| Payout Date  | DATE  | =StakeDate + StakeLength \[1 - 1460 DAYS]                                                                                                                                                      |
