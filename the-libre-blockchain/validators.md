---
description: Libre Chain validator node information.
---

# Validators

The Libre chain is secured by its decentralized network of validators. These validator nodes are independently owned and located all over the world.

<figure><img src="../.gitbook/assets/image (12).png" alt=""><figcaption><p>Libre mainnet node locations as tracked by antelope.tools (see link below)</p></figcaption></figure>

Libre validator nodes provide APIs for submitting transactions, chain history for wallets and applications, as well as validation of blocks for which validators are paid.&#x20;

Validators are paid for each block on the Libre chain that they produce. There is a halvening schedule for block pay which occurs on a superblock. A superblock happens every 15,768,000 seconds. Initially, validators will be paid 4 LIBRE per block and there is a terminal rate of 1 LIBRE per block. There will be 2 halvenings, one at 6 months and one at 12 months.&#x20;

You can learn about the existing nodes by visiting these websites:

* **Libre Mainnet nodes:** [https://libre.antelope.tools](https://libre.antelope.tools/)
* **Libre Testnet nodes:** [https://libre-testnet.antelope.tools](https://libre-testnet.antelope.tools/)

If you want to become a [validator, start here](../earn/become-a-validator.md).
